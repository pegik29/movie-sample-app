import axios from 'axios';
import {api_key} from '../helpers';
axios.defaults.baseURL = process.env.API_BASE_URL;
export const movieService = {
  getAllMovies,
  getMovieDetail,
  getAllGenres,
  getMovieCredits
};

function getAllMovies(data) {
  return axios({
    method: 'get',
    url: 'discover/movie',
    params: {api_key, ...data}
  });
}

function getMovieDetail(movieID) {
  return axios({
    method: 'get',
    url: `movie/${movieID}`,
    params: {api_key}
  });
}

function getAllGenres() {
  return axios({
    method: 'get',
    url: 'genre/movie/list',
    params: {api_key}
  });
}

function getMovieCredits(movieId) {
  return axios({
    method: 'get',
    url: `movie/${movieId}/credits`,
    params: {api_key}
  });
}
