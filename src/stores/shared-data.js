import {defineStore} from 'pinia';
import {movieService} from "src/services";

export const useSharedData = defineStore('sharedData', {
  state: () => ({
    genres: [],
  }),
  getters: {
    // doubleCount: (state) => state.counter * 2,
  },
  actions: {
    async getAllGenres() {
      // this.genres = await movieService.getAllGenres();
      // loading.value = true;
      movieService.getAllGenres()
        .then((result) => {
          this.genres = result.data.genres
        })
        .catch((error) => {
          console.log("getAllGenres error")
          console.log(error)
        })
        .finally(() => {
          // loading.value = false;
        })
    },
    getGenresName(genreIDs) {
      let results = []
      genreIDs.forEach((value) => {
        let temp;
        temp = this.genres.find(element => element.id === value)
        results.push(temp.name)
      })
      return results;
    }
  },
});
