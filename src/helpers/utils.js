export const convertMinsToHrsMins = (mins) => {
  let h = Math.floor(mins / 60);
  let m = mins % 60;
  h = h < 10 ? '0' + h : h; // (or alternatively) h = String(h).padStart(2, '0')
  m = m < 10 ? '0' + m : m; // (or alternatively) m = String(m).padStart(2, '0')
  return `${h}h ${m}m`;
}
export const convertNumToUsd = (value) => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',

    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  });
  return formatter.format(value); /* $2,500.00 */
}
export const ratingColor = (rating) => {
  let color;
  if (rating < 5) {
    color = 'negative'
  } else if (rating < 6.5) {
    color = 'amber'
  } else if (rating < 7.5) {
    color = 'primary'
  } else {
    color = 'accent'
  }
  return color;
}
